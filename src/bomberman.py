"""
Exercise 3:

Bomberman lives in a rectangular grid. Each cell in the grid either contains a bomb or nothing at all.
Each bomb can be planted in any cell of the grid but once planted, it will detonate after exactly 3 seconds.
Once a bomb detonates, it's destroyed — along with anything in its four neighboring cells.
This means that if a bomb detonates in cell i, j, any valid cells ( i ± 1, j ) and ( i, j ± 1 ) are cleared.
If there is a bomb in a neighboring cell, the neighboring bomb is destroyed without detonating,
so there's no chain reaction.

Bomberman is immune to bombs, so he can move freely throughout the grid. Here's what he does:
1.     Initially, Bomberman arbitrarily plants bombs in some of the cells, the initial state.
2.     After one second, Bomberman does nothing.
3.     After one more second, Bomberman plants bombs in all cells without bombs, thus filling the whole grid with bombs.
        No bombs detonate at this point.
4.     After one more second, any bombs planted exactly three seconds ago will detonate.
Here, Bomberman stands back and observes.
5.     Bomberman then repeats steps 3 and 4 indefinitely.
Note that during every second Bomberman plants bombs, the bombs are planted simultaneously (i.e., at the exact
same moment), and any bombs planted at the same time will detonate at the same time.
Given the initial configuration of the grid with the locations of Bomberman's first batch of planted bombs,
determine the state of the grid after N seconds.
For example, if the initial grid looks like:
. . .
. O .
. . .
It looks the same after the first second. After the second second, Bomberman has placed all his charges:
O O O
O O O
O O O
At the third second, the bomb in the middle blows up, emptying all surrounding cells:
O . O
. . .
O . O
Function Description
Create the bomber_man function with following parameter(s):
    - int n: the number of seconds to simulate
    - string grid[r]: an array of string that represents the grid
Returns
    - string[r]: n array of string that represent the grid in its final state

Sample Input:
3
. . . . . . .
. . . O . . .
. . . . O . .
. . . . . . .
O O . . . . .
O O . . . . .

Sample Output
O O O . O O O
O O . . . O O
O O O . . . O
. . O O . O O
. . . O O O O
. . . O O O O

"""
import random


def render_grid(current_second, title, grid_list):
    """Render grid in the console."""
    print(f"\n--------------------- {current_second}. {title}")
    for line in grid_list:
        print(' '.join(
            [char for char in line]
        ))


def plant_bombs_initial(grid):
    """Plant initial bombs."""
    new_grid = []
    for line in grid:
        new_line = ''
        for _ in line:
            # Random 0-3 because we want to populate 30% of cells approximately.
            new_line += '0' if random.randrange(0, 3) == 0 else '.'
        new_grid.append(new_line)

    return new_grid


def planting_more_bombs(grid):
    """Filling empty cells with bombs."""
    new_grid = []
    for line in grid:
        new_line = ''
        for current in line:
            # Convert 2 second bombs into "X" to know what bombs to detonate next.
            new_line += 'X' if current == '0' else '0'
        new_grid.append(new_line)

    return new_grid


def detonate_bombs(grid):
    """Bombs detonate."""

    # Converting grid to a list of arrays, as we can't change elements in strings.
    array = []
    for line in grid:
        array.append([char for char in line])

    for i, row in enumerate(array):
        # print(row)
        for j, char in enumerate(row):
            # print(i, j, char)

            # X means 3 second bomb, we need to detonate it.
            if char == 'X':
                # Current cell becomes empty
                array[i][j] = '.'

                # Get neighbour cells coordinates
                top_x, top_y = (i-1, j)
                bottom_x, bottom_y = (i + 1, j)
                left_x, left_y = (i, j-1)
                right_x, right_y = (i, j+1)

                # If neighbour cells exist and hold 1 second bombs, we remove them.
                if 0 <= top_x < len(array) and 0 <= top_y < len(array[0]) and array[top_x][top_y] == '0':
                    array[top_x][top_y] = '.'
                if 0 <= bottom_x < len(array) and 0 <= bottom_y < len(array[0]) and array[bottom_x][bottom_y] == '0':
                    array[bottom_x][bottom_y] = '.'
                if 0 <= left_x < len(array) and 0 <= left_y < len(array[0]) and array[left_x][left_y] == '0':
                    array[left_x][left_y] = '.'
                if 0 <= right_x < len(array) and 0 <= right_y < len(array[0]) and array[right_x][right_y] == '0':
                    array[right_x][right_y] = '.'

    # Return grid to it's initial format.
    new_grid = []
    for x in array:
        new_grid.append([char for char in x])

    return new_grid


def bomber_man(n, grid):
    """Bomberman's main function.

    :param n: int -- the number of seconds to simulate
    :param grid: list[] -- an array of string that represents the grid
    :return: list -- array of string that represent the grid in its final state
    """
    for x in range(n):
        if x == 0:
            title = 'Planting initial bombs'
            grid = plant_bombs_initial(grid)
        elif x == 1:
            # Do nothing.
            title = 'Do nothing'
            pass
        elif x % 2 == 0:
            # Plant more bombs.
            title = 'Planting bombs'
            grid = planting_more_bombs(grid)
            pass
        else:
            # Bombs detonate.
            title = 'Bombs detonate'
            grid = detonate_bombs(grid)
            pass

        render_grid(x, title, grid)

    # This return does nothing, as we are printing complete scenario :)
    return grid


if __name__ == '__main__':
    seconds = int(input('Enter number of seconds: '))
    grid_width = int(input("Enter grid's width: "))
    grid_height = int(input("Enter grid's height: "))

    # vegar vascreb savarjishos dasrulebas,
    # imedia scori mimartulebit mivdivar :)
    # seconds = 8
    # grid_width = 4
    # grid_height = 4

    grid = []
    for _ in range(grid_height):
        grid.append('.' * grid_width)

    bomber_man(seconds, grid)
