"""
Exercise 2:

Lexicographical order is often known as alphabetical order when dealing with strings.
A string is greater than another string if it comes later in a lexicographically sorted list.

Given a word, create a new word by swapping some or all of its characters.
This new word must meet two criteria:
    - It must be greater than the original word
    - It must be the smallest word that meets the first condition

Example w = abcd

The next largest word is abdc.
Create the function "bigger_is_greater" and return the new string meeting the criteria.
If it is not possible, return no answer.


* Function Description
Function has the following parameter(s):
    - string w: a word
Returns
    - string: the smallest c possible or no answer


* Input Format
The first line of input contains T, the number of test cases. Each of the next T lines contains w.
Constraints
    - 1 ≤ T ≤ 105
    - 1 ≤ length of w ≤ 100
    - w will contain only letters in the range ascii[a...z]


* Sample Input:
    5 ab bb hefg dhck dkhc


* Sample Output
    ba
    no answer
    hegf
    dhkc
    hcdk


* Explanation
Test case 1: ba is the only string which can be made by rearranging ab. It is greater.
Test case 2: It is not possible to rearrange bb and get a greater string.
Test case 3: hegf is the next string greater than hefg.
Test case 4: dhkc is the next string greater than dhck.
Test case 5: hcdk is the next string greater than dkhc.

Sample Input: 6 lmno dcba dcbb abdc abcd fedcbabcd
Sample Output lmon no answer no answer acbd abdc Fedcbabdc

"""


def _get_last_higher_element(w_list):
    i = len(w_list) - 1
    while i >= 0:
        current = w_list[i]
        j = i - 1
        while j >= 0:
            previous = w_list[j]
            if current > previous:
                return i, j
            j -= 1
        i -= 1

    return None


def bigger_is_greater(w):
    """Starting from the end, finds a character that is greater than any element after it.
    Swaps the places and sorts the text after the higher character.

    :param w: str
    :return: str
    """

    w_list = list(w)

    # 1. Get the last higher element and first lower character.
    keys = _get_last_higher_element(w_list)
    if not keys:
        return 'no answer'

    higher = keys[0]
    lower = keys[1]

    # 2. Swap the higher and lower elements.
    w_list[higher], w_list[lower] = w_list[lower], w_list[higher]

    # 3. Sort the text after the higher character
    suffix = w_list[lower + 1:]
    suffix.sort()
    suffix = ''.join(suffix)

    prefix = ''.join(w_list[:lower + 1])

    return prefix + suffix


if __name__ == '__main__':
    num_words = int(input('Enter words number: '))
    for x in range(num_words):
        word = input()
        print(bigger_is_greater(word))
