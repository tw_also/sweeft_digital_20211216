import unittest
from occurrences import count_occurrences_for_tests


class TestTask1(unittest.TestCase):
    def test_1(self):
        test_param = '''4
bcdef
abcdefg
bcde
bcdef
        '''
        result = count_occurrences_for_tests(test_param)
        self.assertEqual(len(result), 3)
        self.assertEqual(result, [2, 1, 1])

    def test_2(self):
        test_param = '''4
abcdefg
bcde
bcdef
bcdef
            '''
        result = count_occurrences_for_tests(test_param)
        self.assertEqual(len(result), 3)
        self.assertEqual(result, [1, 1, 2])

    def test_3(self):
        test_param = ['4', 'bcdef', 'abcdefg', 'bcde', 'bcdef']
        result = count_occurrences_for_tests(test_param)
        self.assertEqual(len(result), 3)
        self.assertEqual(result, [2, 1, 1])

    def test_4(self):
        test_param = '''0
bcdef
abcdefg
bcde
bcdef
        '''
        result = count_occurrences_for_tests(test_param)
        self.assertEqual(len(result), 0)
        self.assertEqual([], result)


unittest.main()
