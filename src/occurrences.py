"""
Exercise 1:

You are given words. Some words may repeat. For each word, output its number of occurrences.
The output order should correspond with the input order of appearance of the word.
See the sample input/output for clarification.

Note: Each input line ends with a "\n" character.

Constraints:
1 ≤ n ≤ 10^5

The sum of the lengths of all the words do not exceed 10^6.
All the words are composed of lowercase English letters only.

* Input Format
The first line contains the integer, n.
The next n lines each contain a word.

* Output Format
Output 2 lines.
On the first line, output the number of distinct words from the input.
On the second line, output the number of occurrences for each distinct word according to their appearance in the input.

* Sample Input:

(The input in the MS Word document was this)
    4
    bcde
    f
    abcd
    efg
    bcde
    bcde
    f


(The input after copy and paste it here was this)
    4 bcdef abcdefg bcde bcdef


(Reading the explanation, I guess the correct input is this?)
    4
    bcdef
    abcdefg
    bcde
    bcdef


* Sample Output
    3
    2 1 1

* Explanation
There are 3 distinct words. Here, "bcdef" appears twice in the input at the first and last positions.
The other words appear once each.
The order of the first appearances are "bcdef", "abcdefg" and "bcde" which corresponds to the output.

"""

# We use ordered dict here, as we need to print occurrences in the same order they were introduced.
from collections import OrderedDict


def count_occurrences():
    """Method for keyboard inputs."""

    my_dict = OrderedDict()

    print('Enter words number:', end='')
    for word in range(int(input())):
        word = input()
        # if word in my_dict:
        #     my_dict[word] += 1
        # else:
        #     my_dict[word] = 1
        my_dict[word] = my_dict.get(word, 0) + 1

    # Print distinct words
    print(len(my_dict))

    # Using starred expression to unpack and print values:
    print(*my_dict.values())


def count_occurrences_for_tests(my_input):
    """Method for unit tests

    :param my_input: list|string
    :return: list
    """
    if type(my_input) == str:
        # In order to accept lists of strings too.
        my_input = my_input.split("\n")

    my_dict = OrderedDict()

    limit = int(my_input[0]) + 1
    for word in my_input[1:limit]:
        my_dict[word] = my_dict.get(word, 0) + 1

    # For unit tests.
    return [*my_dict.values()]


if __name__ == '__main__':
    count_occurrences()
