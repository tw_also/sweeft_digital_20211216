import unittest
from lexicography import bigger_is_greater


class TestTask2(unittest.TestCase):
    def test_1(self):
        result = bigger_is_greater('ab')
        self.assertEqual(result, 'ba')

    def test_2(self):
        result = bigger_is_greater('bb')
        self.assertEqual(result, 'no answer')

    def test_3(self):
        result = bigger_is_greater('hefg')
        # starting from the end:
        # "g" is greater than "f": he{f}{g}
        # swap places between "g" and "f": he{f}{g} = he{g}{f}
        self.assertEqual(result, 'hegf')

    def test_4(self):
        result = bigger_is_greater('dhck')
        # starting from the end:
        # "k" is greater than "c": dh{c}{k}
        # swap places between "k" and "c": dh{c}{k} = dh{k}{c}
        self.assertEqual(result, 'dhkc')

    def test_5(self):
        result = bigger_is_greater('dkhc')
        # starting from the end:
        # "h" is greater than "d": {d}k{h}c
        # Swap places between "h" and "d": {d}k{h}c = {h}k{d}c
        # string after "h" is sorted alphabetically: h{kdc} = h{cdk}
        self.assertEqual(result, 'hcdk')

    def test_6(self):
        result = bigger_is_greater('zdkhc')
        # starting from the end:
        # "c" is not greater than any other character, do nothing.
        # "h" is greater than "d": z{d}k{h}c
        # swap places between "h" and "d": z{d}k{h}c = z{h}k{d}c
        # string after "h" is sored alphabetically: zh{kdc} = zh{cdk}
        self.assertEqual(result, 'zhcdk')

    def test_7(self):
        result = bigger_is_greater('abcd54321')
        # starting from the end:
        # "d" is greater than "c": ab{c}{d}54321
        # Swap places between "d" and "c": ab{c}{d}54321 = ab{d}{c}54321
        # string after "d" is sorted alphabetically: abd{c54321} = abd{12345c}
        self.assertEqual(result, 'abd12345c')

    def test_8(self):
        result = bigger_is_greater('bacd54321')
        self.assertEqual(result, 'bad12345c')

    def test_9(self):
        result = bigger_is_greater('abed54321')
        self.assertEqual(result, 'ad12345be')

    def test_10(self):
        result = bigger_is_greater('abcfdda')
        self.assertEqual(result, 'abdacdf')

    def test_11(self):
        result = bigger_is_greater('abcd5432952')
        # starting from the end:
        # abcd543{2}9{5}2 - find the first element that is greater than any of it's previous characters (5 and 2).
        # abcd543{5}9{2}2 - Swap places between the greater element (5) and first previous character (2).
        # abcd543{5}{922} - The string (922) after the greater character (5) must be sorted alphabetically.
        # abcd5435{229}
        self.assertEqual(result, 'abcd5435229')


unittest.main()
